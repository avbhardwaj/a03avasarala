var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express(); // make express app
var http = require('http').Server(app);

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine
// manage our entries
var entries = [];
app.locals.entries = entries;
// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname + '/assets/')));
app.get("/", function(request, response) {
    response.sendFile(path.join(__dirname + '/assets/Home.html'));
});

app.get("/", function(request, response) {
    response.sendFile(path.join(__dirname + '/assets/Interest.html'));
});

app.get("/", function(request, response) {
    response.sendFile(path.join(__dirname + '/assets/Contact.html'));
});

// GETS
app.get("/", function(request, response) {
    response.render("index");
});
app.get("/guestbook", function(request, response) {
    response.render("index");
});
app.get("/new-entry", function(request, response) {
    response.render("new-entry");
});

// POSTS
app.post("/contact", function(req, res) {
    var api_key = 'key-931ffab98f8d778043047fb013f87a4c';
    var domain = 'sandbox2601cdfc5ccd46d880903818a9e3a957.mailgun.org';
    var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

    var data = {
        from: 'Visioniod Guest Book <postmaster@sandbox2601cdfc5ccd46d880903818a9e3a957.mailgun.org>',
        to: 'bhardwaj@visioniod.com',
        subject: 'A new guest entry by ' + req.body.name,
        text: req.body.message
    };

    mailgun.messages().send(data, function(error, body) {
        console.log(body);
        if (!error)
            res.send("<script> alert('Mail Sent'); location.href='/'; </script>");
        else
            res.send("<script>alert('Mail Not Sent'); location.href='/';</script>");
    });
});
app.post("/new-entry", function(request, response) {
    if (!request.body.title || !request.body.body) {
        response.status(400).send("Entries must have a title and a body.");
        return;
    }
    entries.push({
        title: request.body.title,
        content: request.body.body,
        published: new Date()
    });
    response.redirect("/");
});

// 404
app.use(function(request, response) {
    response.status(404).render("404");
});
// Listen for an application request on port 8081
http.listen(8081, function() {
    console.log('Guestbook app listening on http://127.0.0.1:8081/');
});